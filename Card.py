def empty():
    pass

def receive(p):
    default = {'from': 'reserve', 'to': 'defausse'}
    default.update(p)
    p= default
    print(p)

def draw(p):
    default = {'value': 1}
    default.update(p)
    p=default
    print('draw', p) 

def gain_action(p):
    default = {'value': 1}
    default.update(p)
    p=default
    print('+action', p)
    
def gain_money(p):
    default = {'value': 1}
    default.update(p)
    p=default
    print('+money', p)

def vict_value(p):
    return p['value']


##class Effect:
##    def __init__(self, effect, parameters):
##        self.effect = effect
##        self.parameters = parameters
##
##    def apply(self):
##        self.effect(self.parameters)

class Card:
    def __init__(self, name, img, kinds, cost, effects, events=[], victory=[vict_value, {'value': 0}]):
##        self.actions = {'draw': empty, 'gain_action': empty}
        self.victory = victory
        self.name = name
        self.img = img
        self.kinds = kinds
        self.cost = cost
        self.effects = effects
        self.events = events

    def play(self):
        for effect, parameters in self.effects:
            effect(parameters)
##            self.actions[effect](parameters)

    def victory_points(self):
        return self.victory[0](self.victory[1])

##=Card('', '.png', [''], , [[, {}], [, {'':}]])
    
laboratory=Card('laboratory', 'Laboratory.png', ['Action'], 5, [[draw, {'value': 2}], [gain_action, {}]])
village=Card('village', 'Village.png', ['Action'], 3, [[draw, {}], [gain_action, {'value': 2}]])
##village.actions['draw'] = draw
##village.actions['gain_action'] = gain_action

copper=Card('copper', 'Copper.png', ['Treasure'], 0, [[gain_money, {}]])
silver=Card('silver', 'Silver.png', ['Treasure'], 3, [[gain_money, {'value': 2}]])
gold=Card('gold', 'Gold.png', ['Treasure'], 6, [[gain_money, {'value': 3}]])

estate=Card('estate', 'Estate.png', ['Victory'], 2, [], victory=[vict_value, {'value': 1}])
duchy=Card('duchy', 'Duchy.png', ['Victory'], 5, [], victory=[vict_value, {'value':3}])
province=Card('province', 'Province.png', ['Victory'], 8, [], victory=[vict_value, {'value':6}])



